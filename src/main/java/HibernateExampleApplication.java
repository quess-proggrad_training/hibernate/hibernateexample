import Components.student;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.List;
import java.util.Properties;

public class HibernateExampleApplication {

	private static SessionFactory sessionFactory;
	public static void main(String[] args) {
		/*String ConfigurationFile = "hibernate1.cfg.xml";

		ClassLoader classLoaderObj = HibernateExampleApplication.class.getClassLoader();
		//File f = new File(classLoaderObj.getResource(ConfigurationFile).getFile());
		SessionFactory sessionFactoryObj = new Configuration().configure(new File("hibernate1.cfg.xml")).buildSessionFactory();
		Session sessionObj = sessionFactoryObj.openSession();
		saveRecord(sessionObj);*/
		sessionFactory=	getSessionFactory();
		Session sessionObj = sessionFactory.openSession();
	//	saveRecord(sessionObj);
		Display(sessionObj);
		//Update(sessionObj);
		//Delete(sessionObj);
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/test");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "Anubh@v1");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

				settings.put(Environment.SHOW_SQL, "true");

				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

				settings.put(Environment.HBM2DDL_AUTO, "update");

				configuration.setProperties(settings);
				configuration.addAnnotatedClass(student.class);

				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();

				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}

	private static void saveRecord(Session sessionObj) {
		student s = new student();
		s.setName("Arjun");
		s.setId(3);
		s.setRollno(20);
		s.setStandard(11);
		sessionObj.beginTransaction();
		sessionObj.save(s);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");

	}
	public static void Display(Session sessionObj)
	{
		Query queryObj=sessionObj.createQuery("FROM student");
		List<student> listobj= queryObj.list();
		for(student iterate: listobj)
		{
			System.out.println(iterate.getName()+" "+ iterate.getRollno()+" "+ iterate.getStandard());
		}
	}
	public  static void Update(Session sessionObj)
	{
		int id=1;
		student studentObj=(student)sessionObj.get(student.class,id);
		studentObj.setName("Anubhav");
		studentObj.setStandard(11);
		sessionObj.beginTransaction();
		sessionObj.saveOrUpdate(studentObj);
		sessionObj.getTransaction().commit();
		System.out.println("Update Successful");
	}
	public  static void Delete(Session sessionObj)
	{
		int id=1;
		student studentObj=(student)sessionObj.get(student.class,id);
		sessionObj.beginTransaction();
		sessionObj.delete(studentObj);
		sessionObj.getTransaction().commit();
		System.out.println("Delete Successfull");

	}

}
